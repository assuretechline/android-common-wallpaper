package com.londonbeauty.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.londonbeauty.R;
import com.londonbeauty.global.Utility;
import com.londonbeauty.interfac.ImageClick;
import com.londonbeauty.model.ImageData;
import com.londonbeauty.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by home on 07/07/18.
 */

public class ImageListRecycleAdapter extends RecyclerView.Adapter<ImageListRecycleAdapter.Holder> {

    public ArrayList<ImageData> list;
    Context context;
    ImageLoader imageLoader;
    ImageClick imageClick;

    public ImageListRecycleAdapter(Context context, ArrayList<ImageData> list,ImageClick imageClick) {
        this.list = list;
        this.imageClick=imageClick;
        this.context = context;
        initialize();
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_imagelist, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        setData(holder, position);
        setColor(holder);
    }

    public void setData(Holder holder, final int position) {

        imageLoader.displayImage(list.get(position).getsImage(), holder.imgv_icon, Utility.getImageOptions());
        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              imageClick.clickImage(position);
            }
        });
    }

    public void setColor(Holder holder) {
        //holder.imgv_prop.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imgv_icon;
        LinearLayout ll_main;
        public Holder(View view) {
            super(view);
            imgv_icon = view.findViewById(R.id.imgv_icon);
            ll_main = view.findViewById(R.id.ll_main);

        }
    }
}