package com.londonbeauty.model;

public class ImageData {
    String sImage,oImage;

    public ImageData(String sImage, String oImage) {
        this.sImage = sImage;
        this.oImage = oImage;
    }

    public String getsImage() {
        return sImage;
    }

    public void setsImage(String sImage) {
        this.sImage = sImage;
    }

    public String getoImage() {
        return oImage;
    }

    public void setoImage(String oImage) {
        this.oImage = oImage;
    }
}
