package com.londonbeauty.model;

/**
 * Created by Amisha on 21-Feb-18.
 */

public class MenuData {
    String title;
    int image;
    boolean isSelected;


    public MenuData(String title, int image, boolean isSelected) {
        this.title = title;
        this.image = image;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
