package com.londonbeauty.activity;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.londonbeauty.R;
import com.londonbeauty.adapter.ImageListRecycleAdapter;
import com.londonbeauty.adapter.MenuAdapter;
import com.londonbeauty.api.APIResponseArray;
import com.londonbeauty.api.APIServer;
import com.londonbeauty.global.Utility;
import com.londonbeauty.interfac.ImageClick;
import com.londonbeauty.model.ImageData;
import com.londonbeauty.model.MenuData;
import com.londonbeauty.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;


public class MainActivity extends BaseActivity implements View.OnClickListener, ImageClick {
    Context context;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ListView lstview;
    ImageView imgv_main, imgv_blur, imgv_set_wall, imgv_refresh, imgv_option;
    GifImageView imgv_gif;
    RecyclerView rv_imglist;
    LinearLayout ll_bottom;
    TextView txt_close;

    ImageListRecycleAdapter imageListRecycleAdapter;
    ArrayList<MenuData> menuSetWallpaperList = new ArrayList<>();
    ArrayList<MenuData> menuOptionList = new ArrayList<>();
    ArrayList<MenuData> mainMenuList = new ArrayList<>();

    ArrayList<ImageData> imageDataArrayList = new ArrayList<>();
    ProgressDialog pd;

    MenuAdapter menuAdapter;
    AdView adView;
    int selectedPosition = 0;
    WallpaperManager wallpaperManager;
    NotificationManager notificationManager;

    KProgressHUD hud;

    String ShareApp="";
    String MoreApps="https://play.google.com/store/apps/developer?id=Kishan+Dhameliya";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
        checkRateDialogOpen();
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        wallpaperManager = WallpaperManager.getInstance(context);
    }

    private void setView() {
        lstview = findViewById(R.id.lstview);
        imgv_main = findViewById(R.id.imgv_main);
        rv_imglist = findViewById(R.id.rv_imglist);
        ll_bottom = findViewById(R.id.ll_bottom);
        txt_close = findViewById(R.id.txt_close);
        imgv_gif = findViewById(R.id.imgv_gif);
        imgv_blur = findViewById(R.id.imgv_blur);
        imgv_set_wall = findViewById(R.id.imgv_set_wall);
        imgv_refresh = findViewById(R.id.imgv_refresh);
        imgv_option = findViewById(R.id.imgv_option);
        adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    private void setData() {
        menuAdapter = new MenuAdapter(context, mainMenuList);
        lstview.setAdapter(menuAdapter);

        menuSetWallpaperList.add(new MenuData(context.getString(R.string.HomeSet), 0, false));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            menuSetWallpaperList.add(new MenuData(context.getString(R.string.LockSet), 0, false));
        }

        menuOptionList.add(new MenuData(context.getString(R.string.Download), 0, false));
        menuOptionList.add(new MenuData(context.getString(R.string.Preview), 0, false));
        menuOptionList.add(new MenuData(context.getString(R.string.ShareApp), 0, false));
        menuOptionList.add(new MenuData(context.getString(R.string.MoreApps), 0, false));

        imageListRecycleAdapter = new ImageListRecycleAdapter(context, imageDataArrayList, this);
        rv_imglist.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_imglist.setAdapter(imageListRecycleAdapter);

        if (Utility.isNetworkAvailable(context)) {
            getImage();
        } else {
            Utility.errDialog(context.getString(R.string.network), context);
        }
    }

    private void setListener() {

        txt_close.setOnClickListener(this);
        imgv_set_wall.setOnClickListener(this);
        imgv_option.setOnClickListener(this);
        imgv_refresh.setOnClickListener(this);

        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.HomeSet))) {
                    setImageOnHomeScreen();
                } else if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.LockSet))) {
                    setImageOnLockScreen();
                } else if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.Download))) {
                    Download();
                }else if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.Preview))) {
                    txt_close.performClick();
                    showDialogImg();
                }else if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.ShareApp))) {
                    shareApp();
                }else if (mainMenuList.get(i).getTitle().equals(context.getString(R.string.MoreApps))) {
                    moreApps();
                }
            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == imgv_set_wall) {
            mainMenuList.clear();
            mainMenuList.addAll(menuSetWallpaperList);
            menuAdapter.notifyDataSetChanged();
            Utility.expand(ll_bottom, (int) context.getResources().getDimension(R.dimen._180sdp));
            adView.resume();
        } else if (view == imgv_option) {
            mainMenuList.clear();
            mainMenuList.addAll(menuOptionList);
            menuAdapter.notifyDataSetChanged();
            Utility.expand(ll_bottom, (int) context.getResources().getDimension(R.dimen._180sdp));
            adView.resume();
        } else if (view == txt_close) {
            Utility.collapse(ll_bottom);
            adView.pause();
        } else if (view == imgv_refresh) {
            if (Utility.isNetworkAvailable(context)) {
                getImage();
            } else {
                Utility.errDialog(context.getString(R.string.network), context);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        setBackPressed();
    }

    public void setBackPressed() {
        System.exit(0);
    }

    private void getImage() {
        if (Utility.isNetworkAvailable(context)) {
            imageDataArrayList.clear();

            pd = Utility.showProgressDialog(context);

            apiServer.getImage(new APIResponseArray() {
                @Override
                public void onSuccessArray(JSONArray array) {

                    try {
                        for (int i = 0; i < array.length(); i++) {
                            imageDataArrayList.add(new ImageData(
                                    array.getJSONObject(i).getString("s_image"),
                                    array.getJSONObject(i).getString("o_image")
                            ));
                        }
                        imageListRecycleAdapter.notifyDataSetChanged();

                        if (imageDataArrayList.size() > 0) {
                            clickLoadImage(0);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utility.dismissProgressDialog(pd);
                }

                @Override
                public void onFailureArray(String error) {
                    Utility.dismissProgressDialog(pd);
                }
            });
        } else {
            Utility.errDialog(context.getString(R.string.network), context);
        }
    }

    @Override
    public void clickImage(int position) {
        selectedPosition = position;
        clickLoadImage(position);
    }

    private void clickLoadImage(int position) {
        imgv_gif.setVisibility(View.VISIBLE);
        imgv_main.setVisibility(View.GONE);

        imageLoader.loadImage(imageDataArrayList.get(position).getsImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                Bitmap blurred = blurRenderScript(loadedImage, 25);
                imgv_blur.setImageBitmap(blurred);
            }
        });

        imageLoader.loadImage(imageDataArrayList.get(position).getoImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                imgv_main.setImageBitmap(loadedImage);
                imgv_gif.setVisibility(View.GONE);
                imgv_main.setVisibility(View.VISIBLE);
            }
        });
    }

    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    private void setImageOnLockScreen() {

        imageLoader.loadImage(imageDataArrayList.get(selectedPosition).getoImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                try {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            wallpaperManager.setBitmap(loadedImage, null, true, WallpaperManager.FLAG_LOCK);
                            Toast.makeText(context,  context.getResources().getString(R.string.LockWallpaperSetSuccessfully), Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            wallpaperManager.setBitmap(loadedImage, null, true, WallpaperManager.FLAG_LOCK);
                            Toast.makeText(context, context.getResources().getString(R.string.LockWallpaperSetSuccessfully), Toast.LENGTH_SHORT).show();
                        }
                        e.printStackTrace();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (IOException ee) {
                    ee.printStackTrace();
                }
            }
        });
    }

    private void setImageOnHomeScreen() {
        pd = Utility.showProgressDialog(context);
        imageLoader.loadImage(imageDataArrayList.get(selectedPosition).getoImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                try {
                    Bitmap bitmap = Bitmap.createScaledBitmap(loadedImage, Resources.getSystem().getDisplayMetrics().widthPixels,
                            Resources.getSystem().getDisplayMetrics().heightPixels,
                            true);

                    wallpaperManager.setBitmap(bitmap);
                    Utility.dismissProgressDialog(pd);
                    Toast.makeText(context, context.getResources().getString(R.string.WallpaperSetSuccessfully), Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            wallpaperManager.setBitmap(loadedImage);
                            Toast.makeText(context, context.getResources().getString(R.string.WallpaperSetSuccessfully), Toast.LENGTH_SHORT).show();
                        }
                        Utility.dismissProgressDialog(pd);
                    } catch (Exception ee) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void Download() {
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                .setLabel("Downloading...")
                .setCancellable(false)
                .setMaxProgress(100);

        // execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(context);
        downloadTask.execute(imageDataArrayList.get(selectedPosition).getoImage());

    }

    public class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                File file = new File(Utility.getAppPath(context));
                if (!file.exists()) {
                    file.mkdirs();
                }
                output = new FileOutputStream(Utility.getAppPath(context) + "/" + Utility.getFileNameFromUrl(sUrl[0]));

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
            hud.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            try{
                hud.setProgress(progress[0]);
            }catch (Exception e){
            }
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();

            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.WallpaperDownloaded), Toast.LENGTH_SHORT).show();
                File f = new File(Utility.getAppPath(context) + "/" + Utility.getFileNameFromUrl(imageDataArrayList.get(selectedPosition).getoImage()));
                callMediaScanner(f);

                notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(
                            "channel-01", context.getResources().getString(R.string.app_name),  NotificationManager.IMPORTANCE_HIGH);
                    notificationManager.createNotificationChannel(mChannel);
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "channel-01")
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(context.getResources().getString(R.string.WallpaperDownloaded));

                mBuilder.setAutoCancel(true);
                notificationManager.notify(1, mBuilder.build());

            }
        }
    }

    private void callMediaScanner(File file) {

        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    private void showDialogImg() {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        dialog.setContentView(R.layout.dialog_full_img);
        final ImageView imageView = (ImageView) dialog.findViewById(R.id.imgv_img);
        final GifImageView imgv_gif=dialog.findViewById(R.id.imgv_gif);
        LinearLayout ll_main = (LinearLayout) dialog.findViewById(R.id.ll_main);

        imgv_gif.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);

        ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        imageLoader.loadImage(imageDataArrayList.get(selectedPosition).getoImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                imgv_gif.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(loadedImage);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });


        dialog.show();
    }

    public void shareApp() {
        try {

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");

            //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, GlobalAppConfiguration.share_message);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "" + "\n" + ShareApp);

            context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } catch (Exception e) {
            //e.toString();
        }
    }

    public void moreApps() {
        try {
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/developer?id=Kishan+Dhameliya")));
                return;
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(MoreApps)));
                return;
            }
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void checkRateDialogOpen() {
        if (appPrefrece.getIS_RATE()) {
            if (appPrefrece.getRateCount() == 2) {
                rateAppDialog();
                appPrefrece.setRateCount(0);
            } else {
                appPrefrece.setRateCount(appPrefrece.getRateCount() + 1);
            }
        }
    }

    private void rateAppDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_rating_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txt_no_thanks = (TextView) dialog.findViewById(R.id.txt_no_thanks);
        TextView txt_later = (TextView) dialog.findViewById(R.id.txt_later);
        TextView txt_rate_now = (TextView) dialog.findViewById(R.id.txt_rate_now);


        txt_no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appPrefrece.setIS_RATE(false);
                dialog.dismiss();
            }
        });

        txt_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txt_rate_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appPrefrece.setIS_RATE(false);
                dialog.dismiss();
                Utility.rateApp(context);
            }
        });

        dialog.show();
    }
}

