package com.londonbeauty.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.londonbeauty.R;
import com.londonbeauty.global.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = 2000;
    Context context;
    ImageView imgv_logo;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialize();
        setView();
        setData();
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
    }

    private void setData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Utility.gotoNext(context, MainActivity.class);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
