package com.londonbeauty.api;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jacksonandroidnetworking.JacksonParserFactory;
import com.londonbeauty.sharedPrefrence.AppPrefrece;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Properties;


public class APIServer {

    Context context;
    Properties properties;
    String subUrl = "";
    AppPrefrece appPrefrece;
    String mainUrl = "";


    public APIServer(Context context) {
        this.context = context;
        appPrefrece = new AppPrefrece(context);
        properties = new LoadAssetProperties().loadRESTApiFile(context.getResources(), "rest.properties", context);
        mainUrl = properties.getProperty("MainUrl");
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
    }


    public void getImage(final APIResponseArray listener) {

        AndroidNetworking.get(mainUrl)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {

                    @Override
                    public void onResponse(JSONArray response) {
                        listener.onSuccessArray(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailureArray(anError.getErrorBody());
                    }
                });
    }


}
