package com.londonbeauty.global;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Created by Assure on 15-03-2018.
 */

public class GlobalAppConfiguration extends Application {

    private static GlobalAppConfiguration appInstance;
    Context context;
    public static String auth="simplerestapi",client="frontend-client";


    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";

    public final static int REQUEST_STORAGE = 101;
    public final static int RESULT_TAKE = 102;
    public final static int RESULT_GALLARY = 103;
    public static final int REQUEST_PERMISSION_CAM_STORAGE=1005;


    public static synchronized GlobalAppConfiguration getInstance() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appInstance = this;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);


    }


}
